#include "global.h"
#include "timer1.h"
#include "stepper.h"
#include "song.h"

volatile uint16_t timer1_value;

void timer1_init(void) {
    TIMSK |= _BV(OCIE1A); // Timer/Counter1, Compare A Interrupt Enable
    DDRD |= _BV(PD5);
    TCCR1A |= _BV(COM1A0);
    TCCR1A &= ~_BV(COM1A1);
    TCCR1A &= ~(_BV(WGM11) | _BV(WGM10)); // CTC mode, top = OCR1A
    TCCR1B &= ~_BV(WGM13);
    TCCR1B |= _BV(WGM12);
}

void timer1_start(void) {
#if TIMER1_PRESCALER == 1
    TCCR1B |= _BV(CS10);
#elif TIMER1_PRESCALER == 8
    TCCR1B |= _BV(CS11);
#elif TIMER1_PRESCALER == 64
    TCCR1B |= _BV(CS11) | _BV(CS10);
#elif TIMER1_PRESCALER == 256
    TCCR1B |= _BV(CS12);
#elif TIMER1_PRESCALER == 1024
    TCCR1B |= _BV(CS12) | _BV(CS10);
#endif
}

void timer1_stop(void) {
    TCCR1B &= ~(_BV(CS12) | _BV(CS11) | _BV(CS10));
    stepper_off();
}

void timer1_set(uint16_t cycles) {
    timer1_value = cycles;
}

ISR( TIMER1_COMPA_vect) {
    if (TCCR1B & (_BV(CS12) | _BV(CS11) | _BV(CS10))) {
        stepper_step();
    }

    if (OCR1A != timer1_value) {
        OCR1A = timer1_value;
    }
}
