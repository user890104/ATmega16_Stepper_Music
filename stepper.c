#include "global.h"
#include "stepper.h"

volatile uint8_t step, dir;

#if STEPPER_MODE == 0
uint8_t steps[] PROGMEM = {_BV(PB0) | _BV(PB1), _BV(PB1) | _BV(PB2), _BV(PB2) | _BV(PB3), _BV(PB3) | _BV(PB0)};
#elif STEPPER_MODE == 1
uint8_t steps[] PROGMEM = {_BV(PB0), _BV(PB1), _BV(PB2), _BV(PB3)};
#elif STEPPER_MODE == 2
uint8_t steps[] PROGMEM = { _BV(PB0), _BV(PB0)| _BV(PB1), _BV(PB1), _BV(PB1)| _BV(PB2), _BV(PB2), _BV(PB2)| _BV(PB3), _BV(PB3), _BV(
        PB3)| _BV(PB0) };
#else
uint8_t steps[] PROGMEM = {};
#error Unknown stepper mode!
#endif

void stepper_init(void) {
    DDR(B) |= _BV(PB0) | _BV(PB1) | _BV(PB2) | _BV(PB3);
}

void stepper_off(void) {
    PORT(B) &= ~(_BV(PB0) | _BV(PB1) | _BV(PB2) | _BV(PB3));
}

void stepper_step(void) {
    if (dir) {
        PORT (B) = (PORT(B) & 0xF0) | pgm_read_byte(&steps[step--]);

        if (step >= STEPPER_NUM_STEPS) {
            step = STEPPER_NUM_STEPS - 1;
        }
    }
    else {
        PORT (B) = (PORT(B) & 0xF0) | pgm_read_byte(&steps[step++]);

        if (step >= STEPPER_NUM_STEPS)
            step = 0;
    }
}

void stepper_toggle_dir(void) {
    dir = 1 - dir;
}
