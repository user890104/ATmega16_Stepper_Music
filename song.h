#ifndef SONG_H_
#define SONG_H_

#include "timer1.h"

#define FREQ2CYCLES(freq) ((freq) ? F_CPU / TIMER1_PRESCALER / freq: 0)
#define NOTELEN2TENMS(len) (20000 / len)
#define NOTE(freq, len) { FREQ2CYCLES(freq) * 4, NOTELEN2TENMS(len) }

void song_init(void);
void song_next_note(void);
void song_play(void);

#endif /* SONG_H_ */
