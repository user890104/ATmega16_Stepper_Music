#include "global.h"
#include "stepper.h"
#include "song.h"
#include "timer0.h"
#include "timer1.h"

#include "notes.h"

volatile uint16_t curr_note;

void song_init(void) {
    timer0_init();
    timer0_set(0x9B);
    timer1_init();
    song_next_note();
}

void song_next_note(void) {
    if (curr_note > sizeof(notes) / 4 - 1) {
        curr_note = 0;
    }

    uint16_t curr_freq = pgm_read_word(&notes[curr_note][0]);

    if (curr_freq) {
        timer1_stop();
        stepper_toggle_dir();
        timer1_set(curr_freq);
        timer1_start();
    }
    else {
        timer1_stop();
    }

    timer0_set_counter(pgm_read_word(&notes[curr_note][1]));

    ++curr_note;
}

void song_play(void) {
    timer1_start();
    _delay_ms(1000);
    timer0_start();
}
