#include "global.h"
#include "stepper.h"
#include "song.h"

int8_t main(void) {
    //set_sleep_mode(SLEEP_MODE_IDLE);
    stepper_init();
    song_init();
    sei();
    song_play();

    while (1) {
        sleep_mode();
    }

    return 0;
}
