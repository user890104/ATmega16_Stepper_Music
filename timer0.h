#ifndef TIMER0_H_
#define TIMER0_H_

#define TIMER0_PRESCALER 1024

void timer0_init(void);
void timer0_start(void);
void timer0_stop(void);
void timer0_set(uint8_t cycles);
void timer0_set_counter(uint16_t cycles);

#endif /* TIMER0_H_ */
