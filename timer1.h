#ifndef TIMER1_H_
#define TIMER1_H_

#define TIMER1_PRESCALER 256

void timer1_init(void);
void timer1_start(void);
void timer1_stop(void);
void timer1_set(uint16_t cycles);

#endif /* TIMER1_H_ */
