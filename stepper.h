#ifndef STEPPER_H_
#define STEPPER_H_

#define STEPPER_MODE 2

#if STEPPER_MODE < 2
#define STEPPER_NUM_STEPS 4
#elif STEPPER_MODE == 2
#define STEPPER_NUM_STEPS 8
#else
#define STEPPER_NUM_STEPS 0
#error Unknown stepper mode!
#endif

void stepper_init(void);
void stepper_off(void);
void stepper_step(void);
void stepper_toggle_dir(void);

#endif /* STEPPER_H_ */
