#include "global.h"
#include "timer0.h"
#include "stepper.h"
#include "song.h"

volatile uint8_t timer0_value;
volatile uint16_t timer0_counter, timer0_counter_top;

void timer0_init(void) {
    TIMSK |= _BV(OCIE0); // Timer/Counter0, Compare Interrupt Enable
    TCCR0 |= _BV(WGM01); // CTC mode, top = OCR0
    TCCR0 &= ~_BV(WGM00);
}

void timer0_start(void) {
#if TIMER0_PRESCALER == 1
    TCCR0 |= _BV(CS00);
#elif TIMER0_PRESCALER == 8
    TCCR0 |= _BV(CS01);
#elif TIMER0_PRESCALER == 64
    TCCR0 |= _BV(CS01) | _BV(CS00);
#elif TIMER0_PRESCALER == 256
    TCCR0 |= _BV(CS02);
#elif TIMER0_PRESCALER == 1024
    TCCR0 |= _BV(CS02) | _BV(CS00);
#endif
}

void timer0_stop(void) {
    TCCR0 &= ~(_BV(CS02) | _BV(CS01) | _BV(CS00));
}

void timer0_set(uint8_t cycles) {
    OCR0 = timer0_value;
}

void timer0_set_counter(uint16_t ten_ms_count) {
    timer0_counter_top = ten_ms_count;
}

ISR( TIMER0_COMP_vect) {
    if (timer0_counter > timer0_counter_top) {
        song_next_note();
        timer0_counter = 0;
    }
    else {
        ++timer0_counter;
    }
}
